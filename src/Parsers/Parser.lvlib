﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Ctrls" Type="Folder">
		<Item Name="Data Cluster.ctl" Type="VI" URL="../Data Cluster.ctl"/>
		<Item Name="Data.ctl" Type="VI" URL="../Data.ctl"/>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Item Name="Data Arrays to Data Clusters.vi" Type="VI" URL="../Data Arrays to Data Clusters.vi"/>
		<Item Name="File Dialog.vi" Type="VI" URL="../File Dialog.vi"/>
		<Item Name="Find Unique Headers.vi" Type="VI" URL="../Find Unique Headers.vi"/>
		<Item Name="Read and parse directory.vi" Type="VI" URL="../Read and parse directory.vi"/>
		<Item Name="Read Files in Directories.vi" Type="VI" URL="../Read Files in Directories.vi"/>
	</Item>
	<Item Name="CSV Data Array to Data Cluster.vi" Type="VI" URL="../CSV Data Array to Data Cluster.vi"/>
	<Item Name="File Dialog CSV.vi" Type="VI" URL="../File Dialog CSV.vi"/>
	<Item Name="Generic Data Parser.vi" Type="VI" URL="../Generic Data Parser.vi"/>
	<Item Name="Read CSV.vi" Type="VI" URL="../Read CSV.vi"/>
	<Item Name="Swivel Data Parser.vi" Type="VI" URL="../Swivel Data Parser.vi"/>
</Library>
